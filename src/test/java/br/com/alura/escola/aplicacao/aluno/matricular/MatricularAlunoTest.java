package br.com.alura.escola.aplicacao.aluno.matricular;

import static org.junit.jupiter.api.Assertions.assertEquals;

import br.com.alura.escola.dominio.aluno.Aluno;
import br.com.alura.escola.dominio.aluno.CPF;
import br.com.alura.escola.dominio.aluno.RepositorioDeAlunos;
import br.com.alura.escola.infra.aluno.RepositorioDeAlunosEmMemoria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MatricularAlunoTest {

  MatricularAluno matricularAluno;
  RepositorioDeAlunos repositorioDeAlunos;

  @BeforeEach
  void setup() {
    repositorioDeAlunos = new RepositorioDeAlunosEmMemoria();
    matricularAluno = new MatricularAluno(repositorioDeAlunos);
  }


  @Test
  void alunoDeveriaSerPersistido() {
    MatricularAlunoDto alunoPersistido = new MatricularAlunoDto(
        "Alguém",
        "123.456.789-10",
        "examplo@exemplo.com");
    matricularAluno.executa(alunoPersistido);

    Aluno alunoEncontrado = repositorioDeAlunos
        .buscarPorCPF(new CPF(alunoPersistido.getCpfAluno()));
    assertEquals(alunoPersistido.getCpfAluno(), alunoEncontrado.getCpf());
    assertEquals(alunoPersistido.getNomeAluno(), alunoEncontrado.getNome());
    assertEquals(alunoPersistido.getEmailAluno(), alunoEncontrado.getEmail());
  }
}