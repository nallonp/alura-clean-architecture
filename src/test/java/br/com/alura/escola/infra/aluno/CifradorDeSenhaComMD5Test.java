package br.com.alura.escola.infra.aluno;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CifradorDeSenhaComMD5Test {

  CifradorDeSenhaComMD5 cifradorDeSenhaComMD5;
  String senhaCifrada;
  String senhaSimples;

  @BeforeEach
  void setup() {
    cifradorDeSenhaComMD5 = new CifradorDeSenhaComMD5();
    senhaCifrada = "698dc19d489c4e4db73e28a713eab07b";
    senhaSimples = "teste";
  }

  @Test
  void oCifradorDeveriaRetornarUmaSenhaIgualAQueFoiPreviamenteCifrada() {
    assertEquals(senhaCifrada, cifradorDeSenhaComMD5.cifrarSenha(senhaSimples));
  }

  @Test
  void aSenhaSimplesDeveriaSerValidada() {
    assertTrue(cifradorDeSenhaComMD5.validarSenhaCifrada(senhaCifrada, senhaSimples));
  }

}