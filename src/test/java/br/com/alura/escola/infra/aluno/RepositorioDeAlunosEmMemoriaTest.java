package br.com.alura.escola.infra.aluno;

import static org.junit.jupiter.api.Assertions.assertEquals;

import br.com.alura.escola.dominio.aluno.Aluno;
import br.com.alura.escola.dominio.aluno.FabricaDeAluno;
import br.com.alura.escola.dominio.aluno.RepositorioDeAlunos;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RepositorioDeAlunosEmMemoriaTest {

  RepositorioDeAlunos repositorioDeAlunos;
  Aluno aluno;

  @BeforeEach
  void setUp() {
    repositorioDeAlunos = new RepositorioDeAlunosEmMemoria();
    aluno = new FabricaDeAluno(
        "Fulano de Alguma Coisa",
        "123.456.789-10",
        "fulano@exemplo.com")
        .criar();
  }

  @Test
  void deveriaMatricularAluno() {
    repositorioDeAlunos.matricular(aluno);
    assertEquals(1, repositorioDeAlunos.listarTodosOsAlunosMatriculados().size());
  }

  @Test
  void buscarPorCPF() {
  }

  @Test
  void listarTodosOsAlunosMatriculados() {
  }
}