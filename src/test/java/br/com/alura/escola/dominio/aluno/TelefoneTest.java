package br.com.alura.escola.dominio.aluno;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class TelefoneTest {

  @Test
  void naoDeveriaCriarTelefoneComDDDInvalido() {
    assertThrows(IllegalArgumentException.class, () -> new Telefone(null, "99999999"));
    assertThrows(IllegalArgumentException.class, () -> new Telefone("", "99999999"));
    assertThrows(IllegalArgumentException.class, () -> new Telefone("ABC", "99999999"));
  }

  @Test
  void naoDeveriaCriarTelefoneComNumeroInvalido() {
    assertThrows(IllegalArgumentException.class, () -> new Telefone("99", null));
    assertThrows(IllegalArgumentException.class, () -> new Telefone("99", ""));
    assertThrows(IllegalArgumentException.class, () -> new Telefone("99", "ABC"));
  }

  @Test
  void deveriaCriarTelefoneComNumeroValido() {
    assertAll(() -> new Telefone("99", "99999999"));
    assertAll(() -> new Telefone("99", "999999999"));
  }

}