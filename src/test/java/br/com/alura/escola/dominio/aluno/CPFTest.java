package br.com.alura.escola.dominio.aluno;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class CPFTest {

  @Test
  void naoDeveriaCriarCpfComNumeroInvalido() {
    assertThrows(IllegalArgumentException.class, () -> new CPF(null));
    assertThrows(IllegalArgumentException.class, () -> new CPF(""));
    assertThrows(IllegalArgumentException.class, () -> new CPF("999.999"));
  }

  @Test
  void deveriaCriarCpfComNumeroInvalido() {
    assertAll(() -> new CPF("999.999.999-99"));
  }
}