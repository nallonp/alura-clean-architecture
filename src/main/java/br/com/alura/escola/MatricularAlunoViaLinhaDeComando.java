package br.com.alura.escola;

import br.com.alura.escola.aplicacao.aluno.matricular.MatricularAluno;
import br.com.alura.escola.aplicacao.aluno.matricular.MatricularAlunoDto;
import br.com.alura.escola.infra.aluno.RepositorioDeAlunosEmMemoria;

public class MatricularAlunoViaLinhaDeComando {

  public static void main(String[] args) {
    String nome = "Fulano da Silva";
    String cpf = "123.456.789-10";
    String email = "teste@exemplo.com";

    MatricularAluno matricularAluno = new MatricularAluno(new RepositorioDeAlunosEmMemoria());
    matricularAluno.executa(new MatricularAlunoDto(nome, cpf, email));
  }
}
