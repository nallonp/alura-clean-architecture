package br.com.alura.escola.aplicacao.aluno.matricular;

import br.com.alura.escola.dominio.aluno.Aluno;
import br.com.alura.escola.dominio.aluno.CPF;
import br.com.alura.escola.dominio.aluno.Email;

public class MatricularAlunoDto {

  private String nomeAluno;
  private String cpfAluno;
  private String emailAluno;

  public MatricularAlunoDto(String nome, String cpf, String email) {
    nomeAluno = nome;
    cpfAluno = cpf;
    emailAluno = email;
  }

  public Aluno criarAluno() {
    return new Aluno(new CPF(cpfAluno), nomeAluno, new Email(emailAluno));
  }

  public String getNomeAluno() {
    return nomeAluno;
  }

  public String getCpfAluno() {
    return cpfAluno;
  }

  public String getEmailAluno() {
    return emailAluno;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    MatricularAlunoDto that = (MatricularAlunoDto) o;

    if (!nomeAluno.equals(that.nomeAluno)) {
      return false;
    }
    if (!cpfAluno.equals(that.cpfAluno)) {
      return false;
    }
    return emailAluno.equals(that.emailAluno);
  }

  @Override
  public int hashCode() {
    int result = nomeAluno.hashCode();
    result = 31 * result + cpfAluno.hashCode();
    result = 31 * result + emailAluno.hashCode();
    return result;
  }
}
