package br.com.alura.escola.infra.aluno;

import br.com.alura.escola.dominio.aluno.CifradorDeSenha;
import java.security.MessageDigest;

public class CifradorDeSenhaComMD5 implements CifradorDeSenha {

  @Override
  public String cifrarSenha(String senha) {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("MD5");
      messageDigest.update(senha.getBytes());
      byte[] bytes = messageDigest.digest();
      StringBuilder stringBuilder = new StringBuilder();
      for (byte b : bytes) {
        stringBuilder.append(String.format("%02x", b & 0xff));
      }
      return stringBuilder.toString();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public boolean validarSenhaCifrada(String senhaCifrada, String senhaAberta) {
    return senhaCifrada.equals(cifrarSenha(senhaAberta));
  }
}
