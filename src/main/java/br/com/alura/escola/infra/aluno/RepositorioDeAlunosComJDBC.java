package br.com.alura.escola.infra.aluno;

import br.com.alura.escola.dominio.aluno.Aluno;
import br.com.alura.escola.dominio.aluno.AlunoNaoEncontrado;
import br.com.alura.escola.dominio.aluno.CPF;
import br.com.alura.escola.dominio.aluno.FabricaDeAluno;
import br.com.alura.escola.dominio.aluno.RepositorioDeAlunos;
import br.com.alura.escola.dominio.aluno.Telefone;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class RepositorioDeAlunosComJDBC implements RepositorioDeAlunos {

  private final Connection connection;

  public RepositorioDeAlunosComJDBC(Connection connection) {
    this.connection = connection;
  }

  @Override
  public void matricular(Aluno aluno) {
    try {
      String sql = "INSERT INTO ALUNO VALUES (?, ?, ?)";
      PreparedStatement ps = connection.prepareStatement(sql);
      ps.setString(1, aluno.getCpf());
      ps.setString(2, aluno.getNome());
      ps.setString(3, aluno.getEmail());
      ps.execute();
      sql = "INSERT INTO TELEFONE VALUES (?, ?, ?)";
      ps = connection.prepareStatement(sql);
      ps.setString(1, aluno.getCpf());
      for (Telefone telefone : aluno.getTelefones()) {
        ps.setString(2, telefone.getDdd());
        ps.setString(3, telefone.getNumero());
        ps.execute();
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Aluno buscarPorCPF(CPF cpf) {
    try {
      String sql = ""
          + "SELECT cpf, nome, email, ddd, numero "
          + "FROM ALUNO "
          + "LEFT JOIN TELEFONE "
          + "ON ALUNO.CPF = TELEFONE.CPF "
          + "WHERE ALUNO.CPF = (?)";
      PreparedStatement ps = connection.prepareStatement(sql);
      ResultSet resultSet = ps.executeQuery();
      if (!resultSet.isBeforeFirst()) {
        throw new AlunoNaoEncontrado(cpf);
      }
      FabricaDeAluno fabricaDeAluno = new FabricaDeAluno(
          resultSet.getString("nome"),
          resultSet.getString("cpf"),
          resultSet.getString("email"));
      while (resultSet.next()) {
        fabricaDeAluno.comTelefone(
            resultSet.getString("ddd"),
            resultSet.getString("numero")
        );
      }
      return fabricaDeAluno.criar();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public List<Aluno> listarTodosOsAlunosMatriculados() {
    try {
      ArrayList<Aluno> alunos = new ArrayList<>();
      String sql = ""
          + "SELECT cpf, nome, email, ddd, numero "
          + "FROM ALUNO "
          + "LEFT JOIN TELEFONE "
          + "ON ALUNO.CPF = TELEFONE.CPF";
      PreparedStatement ps = connection.prepareStatement(sql);
      ResultSet resultSet = ps.executeQuery();
      FabricaDeAluno fabricaDeAluno = new FabricaDeAluno(
          resultSet.getString("nome"),
          resultSet.getString("cpf"),
          resultSet.getString("email"));
      while (resultSet.next()) {
        fabricaDeAluno.comTelefone(
            resultSet.getString("ddd"),
            resultSet.getString("numero")
        );
        alunos.add(fabricaDeAluno.criar());
      }
      return alunos;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
